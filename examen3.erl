 % http://127.0.0.1:8000/api/examen3/clienteJson/numero=10/tipo=circulo
 % http://127.0.0.1:8000/api/examen3/clienteJson/numero=10/tipo=cuadrado
 % validate even numbers from an erlang list

-module(examen3).

-export([start/0, clienteJson/3]).

start() ->
    inets:start(httpd,
                [{modules,
                  [mod_alias,
                   mod_auth,
                   mod_esi,
                   mod_actions,
                   mod_cgi,
                   mod_dir,
                   mod_get,
                   mod_head,
                   mod_log,
                   mod_disk_log]},
                 {port, 8000},
                 {server_name, "shell"},
                 {server_root, "/home/celi/Documentos/erl"},
                 {document_root, "/home/celi/Documentos/erl"},
                 {erl_script_alias, {"/api", [examen3]}},
                 {error_log, "error.log"},
                 {security_log, "security.log"},
                 {transfer_log, "transfer.log"},
                 {mime_types,
                  [{"html", "text/html"},
                   {"css", "text/css"},
                   {"js", "application/x-javascript"},
                   {"json", "application/json"}]}]).

clienteJson(SessionID, _Env, _Input) ->
    [_, Fin] = string:split(_Input, "numero="),
    [In2, Fin2] = string:split(Fin, "/tipo="),
    Num = list_to_integer(In2),
    Ar = generarLista(Num),
    io:format("data: ~w~n", [Ar]),
    ArregloPares = identificarPares(Ar),
    io:format("arreglo de pares: ~p~n", [ArregloPares]),
    Sumadepares = sumarArregloPares(ArregloPares),
    io:format("suma de pares: ~p~n", [Sumadepares]),

    Result =
        if Fin2 == "cuadrado" ->
               calcularSuperficieCuadrado(Sumadepares);
           Fin2 == "circulo" ->
               calcularSuperficieCirculo(Sumadepares)
        end,
    io:format("resultado: ~p~n", [Result]),

    B = io_lib:format("~p", [ArregloPares]),
    SumP = io_lib:format("~p", [Sumadepares]),
    R = io_lib:format("~p", [Result]),
    Str1 = "\"Lista de pares es\":\"",
    Str2 = "\"La suma de pares es\":\"",
    Str3 = "\"La superficie del "++ Fin2 ++" es\":\"",
    mod_esi:deliver(SessionID,
                    ["Content-Type: application/json\r\n\r\n",
                     [Str1, B],
                     [Str2, SumP],
                     [Str3, R]]).

generarLista(Num) ->
    Lista = [rand:uniform(1000) || _ <- lists:seq(1, Num)],
    Lista.

identificarPares([]) ->
    [];
identificarPares([H | T]) when H rem 2 /= 0 ->
    identificarPares(T);
identificarPares([H | T]) ->
    [H | identificarPares(T)].

sumarArregloPares(List) ->
    sumarArregloPares(List, 0).

sumarArregloPares([H | T], C) ->
    sumarArregloPares(T, C + H);
sumarArregloPares([], C) ->
    C.

calcularSuperficieCirculo(Sumadepares) ->
    Resul = math:pi() * Sumadepares * Sumadepares,
    Resul.

calcularSuperficieCuadrado(Sumadepares) ->
    Resul = Sumadepares * Sumadepares,
    Resul.
